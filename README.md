# gitlab-ci-playground

## This example isn't working as code quality does not work with free accounts

Example project to explore gitlab ci features and share with others

## Whats inside this project
- 	README.md this document
- 	.gitlab-ci.yml  Gitlab Continuos Integration Pipeline configuration file
-	src/ Test files to try php-cs and pipeline
-	.gitlab/ci/ special directory to store tools to run in pipeline
-	.gitlab/ci/run_phpcs.sh Main script for pipeline
-	.gitlab/ci/Sniffs.php Codesniffer Issue Collection
-	.gitlab/ci/code_quality_report.php Convert codesniffer report to codeclimate report
